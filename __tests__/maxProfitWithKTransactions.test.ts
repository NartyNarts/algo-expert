import { maxProfitWithKTransactions } from '../src/maxProfitWithKTransactions';

describe('maxProfitWithKTransactions', () => {
  it('should return 0 if prices is empty', () => {
    expect(maxProfitWithKTransactions([], 1)).toBe(0);
  });

  it('should return 0 if k is 0', () => {
    expect(maxProfitWithKTransactions([1, 2, 3, 4, 5], 0)).toBe(0);
  });

  it('should return 0 if prices.length < 2', () => {
    expect(maxProfitWithKTransactions([1], 1)).toBe(0);
  });

  it('should return the max profit if prices.length >= 2 and k >= 1', () => {
    expect(maxProfitWithKTransactions([1, 2, 3, 4, 5], 1)).toBe(4);
    expect(maxProfitWithKTransactions([1, 2, 3, 4, 5], 2)).toBe(4);
    expect(maxProfitWithKTransactions([1, 2, 3, 4, 5], 3)).toBe(4);
    expect(maxProfitWithKTransactions([1, 2, 3, 4, 5], 4)).toBe(4);
    expect(maxProfitWithKTransactions([1, 2, 3, 4, 5], 5)).toBe(4);
    expect(maxProfitWithKTransactions([1, 10], 3)).toBe(9);
    expect(maxProfitWithKTransactions([3, 2, 5, 7, 1, 3, 7], 1)).toBe(6);
    expect(maxProfitWithKTransactions([5, 11, 3, 50, 60, 90], 2)).toBe(93);
  });
});